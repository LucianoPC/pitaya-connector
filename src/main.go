package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/acceptor"
	"github.com/topfreegames/pitaya/logger"
	"github.com/topfreegames/pitaya/serialize/json"
	"gitlab.com/lucianopc/pitaya-connector/bll/router"
	"gitlab.com/lucianopc/pitaya-connector/constants"
)

func main() {
	config, err := initializeConfig()
	if err != nil {
		fmt.Printf("Config file %s failed to load: %s.\n", "config.yaml", err.Error())
		panic("Failed to load config file")
	}

	logger := initializeLogger(config)

	defer pitaya.Shutdown()
	pitaya.SetSerializer(json.NewSerializer())

	configureConnector(logger, config)

	svType := constants.ServerSvType
	isFrontend := constants.ServerIsFrontend
	serverMetadata := map[string]string{}

	pitaya.Configure(isFrontend, svType, pitaya.Cluster, serverMetadata, config)
	pitaya.Start()
}

func configureConnector(logger logrus.FieldLogger, config *viper.Viper) error {
	port := config.GetInt32("server.port")
	keyFile := ""
	certFile := ""

	tcp := acceptor.NewTCPAcceptor(fmt.Sprintf(":%d", port), certFile, keyFile)

	err := pitaya.SetDictionary(map[string]uint16{})
	if err != nil {
		return err
	}
	pitaya.AddAcceptor(tcp)

	source := rand.NewSource(time.Now().Unix())
	routingFunc, err := router.RoutingFunc(logger, config, source)
	if err != nil {
		return err
	}

	routes := config.GetStringSlice("server.routes")
	for _, route := range routes {
		pitaya.AddRoute(route, routingFunc)
	}

	return nil
}

func initializeConfig() (*viper.Viper, error) {
	config := viper.New()
	config.SetConfigFile("config.yaml")
	config.SetConfigType("yaml")
	config.SetEnvPrefix("")
	config.AddConfigPath(".")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	err := config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	return config, nil
}

func initializeLogger(config *viper.Viper) logrus.FieldLogger {
	log := logrus.New()

	verbose := config.GetString("log.verbose")

	switch verbose {
	case "error":
		log.Level = logrus.ErrorLevel
	case "warning":
		log.Level = logrus.WarnLevel
	case "info":
		log.Level = logrus.InfoLevel
	case "debug":
		log.Level = logrus.DebugLevel
	default:
		log.Level = logrus.DebugLevel
	}

	serverID := config.GetString("server.id")
	serverVersion := config.GetString("server.version")

	fieldLogger := log.WithFields(logrus.Fields{
		"source":  serverID,
		"server":  constants.ServerSvType,
		"version": serverVersion,
	})

	logger.SetLogger(fieldLogger)

	return fieldLogger
}
