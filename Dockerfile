FROM golang:1.14.1

RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

ADD . /go/src/gitlab.com/lucianopc/pitaya-connector
WORKDIR /go/src/gitlab.com/lucianopc/pitaya-connector

RUN dep ensure -v

RUN cp -r vendor/ /vendor

EXPOSE 3250
ENTRYPOINT ["./entrypoint.sh"]
CMD ["go", "run", "src/main.go"]