package statuscodes

import (
	"errors"
)

var (
	ErrNoServerFound = errors.New("Error no server not founded")
)
