package statuscodes

import (
	"github.com/topfreegames/pitaya"
	pitayaErrors "github.com/topfreegames/pitaya/errors"
)

type StatusCode string

var (
	CodeInternal           StatusCode = "CON-500"
	CodeServiceUnavailable StatusCode = "CON-503"
)

func (s StatusCode) String() string {
	return string(s)
}

func Error(err error, code StatusCode, metadata ...map[string]string) *pitayaErrors.Error {
	return pitaya.Error(err, code.String(), metadata...)
}
