package router

import (
	"context"
	"math/rand"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/cluster"
	"github.com/topfreegames/pitaya/route"
	"github.com/topfreegames/pitaya/router"
	"gitlab.com/lucianopc/pitaya-connector/statuscodes"
)

func RoutingFunc(logger logrus.FieldLogger, config *viper.Viper, source rand.Source) (router.RoutingFunc, error) {
	logger = logger.WithFields(logrus.Fields{
		"source": "bll.router",
		"method": "RoutingFunc",
	})
	logger.Info("Adding router")

	return func(ctx context.Context, r *route.Route, _ []byte, servers map[string]*cluster.Server) (*cluster.Server, error) {
		session := pitaya.GetSessionFromCtx(ctx)

		logger = logger.WithField("route", r.String())
		logger.Debugf("routing for client: %+v", session.GetHandshakeData())

		possibleServers := []*cluster.Server{}
		for _, server := range servers {
			possibleServers = append(possibleServers, server)
		}

		hasServerToRoute := len(possibleServers) > 0

		if !hasServerToRoute {
			return nil, statuscodes.Error(
				statuscodes.ErrNoServerFound,
				statuscodes.StatusCode(statuscodes.CodeServiceUnavailable))
		}

		server := chooseRandomServer(possibleServers, source)

		return server, nil
	}, nil
}

func chooseRandomServer(possibleServers []*cluster.Server, source rand.Source) *cluster.Server {
	rnd := rand.New(source)
	return possibleServers[rnd.Intn(len(possibleServers))]
}
